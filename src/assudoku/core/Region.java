package assudoku.core;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;


/**
 * Region class: represents a region in a sudoku puzzle comprising n^2 Cells for
 * a puzzle of size n
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class Region {

	private final SudokuPuzzle parentSudokuPuzzle;
	private final int regionId;
	private final String[] validValues; // List of valid values for this game
	private List<Cell> cellList;

	// maintain master list of Cell objects; only one Cell object exists for any
	// unique (x,y) coordinate
	// but in a harder sudoku game with overlapping Regions, a single Cell
	// object can belong to several
	// Regions. When constructing/adding new Cell objects from the
	// GameInputParser, Region checks the
	// CellMasterList to ensure that the Cell does not already exist before
	// creating it; if it does already
	// exist, it adds a reference to the existing object rather than creating a
	// brand new object with the
	// same information.
	private final CellMasterList cellMasterList;

	/**
	 * Construct a Region object.
	 * 
	 * @param validValues
	 *            the valid values of this Sudoku game.
	 */
	public Region(SudokuPuzzle parentSudokuPuzzle, int regionId,
			String[] validValues, CellMasterList cellMasterList) {
		this.parentSudokuPuzzle = parentSudokuPuzzle;
		this.regionId = regionId;
		this.validValues = validValues;
		this.cellList = new ArrayList<Cell>();
		this.cellMasterList = cellMasterList;
	}

	/**
	 * Get SudokuPuzzle that this Region belongs to
	 * 
	 * @return SudokuPuzzle object that contains this Region
	 */
	public SudokuPuzzle getParentSudokuPuzzle() {
		return this.parentSudokuPuzzle;
	}

	/**
	 * Get this region's ID
	 * 
	 * @return int representing region ID
	 */
	public int getRegionId() {
		return this.regionId;
	}

	/**
	 * Get this region's cellList
	 * 
	 * @return list of Cell objects belonging to this region
	 */
	public List<Cell> getCellList() {
		return this.cellList;
	}

	/**
	 * Add a Cell object to this Region's cellList
	 * 
	 * @param x
	 *            coordinate of cell
	 * @param y
	 *            coordinate of cell
	 * @param value
	 *            String value of cell
	 * @param isEditable
	 *            whether the cell can be edited by the GUI
	 */
	public void addCell(int x, int y, String value, boolean isEditable) {
		// check if cell with coordinates (x, y) already exists in the puzzle
		Cell cell = this.cellMasterList.findExistingCell(x, y);
		// if it doesn't exist, first create a new cell and add it to
		// cellMasterList
		if (cell == null) {
			cell = new Cell(x, y, value, isEditable);
			this.cellMasterList.add(cell);
		}
		// add the cell to this region's list of cells
		// add a reference to this region to in the cell
		this.cellList.add(cell);
		cell.addParentRegion(this);
	}

	/**
	 * Check whether this Region contains valid cell values for the game logic
	 * ie. do the cell values appear in validValues and are they unique for this
	 * region Sets cell flags for region correctness in each of this region's
	 * cells
	 * 
	 * @return true if the Region's cells are valid as per the game logic
	 */
	public boolean isCorrect() {
		boolean correct = true;
		// if i is visited, validValuesFlag[i]==1
		boolean[] validValuesFlag = new boolean[validValues.length];
		Iterator<Cell> cit = cellList.iterator();
		while (cit.hasNext() && correct) {
			Cell c = cit.next();
			for (int i = 0; i < validValues.length; i++) {
				if (validValues[i].equals(c.getValue())) {
					if (validValuesFlag[i] == true) {
						correct = false;
					}
					validValuesFlag[i] = true;
				}
			}
		}
		// sets all of the cells in this Region to match the Region's
		// correctness value
		// true for correct, false for incorrect
		for (Cell cell : cellList) {
			cell.setRegionIsCorrect(correct);
		}
		return correct;
	}

	/**
	 * Set cell flags for x (column) correctness in all cells in this Region
	 * with a given x coordinate
	 * 
	 * @param x
	 *            coordinate of a cell
	 * @param flag
	 *            set to either true or false
	 */
	public void setCellXCorrect(int x, boolean flag) {
		for (Cell c : cellList) {
			if (c.getX() == x) {
				c.setXIsCorrect(flag);
			}
		}
	}

	/**
	 * Set cell flags for y (row) correctness in all cells in this Region with a
	 * given y coordinate
	 * 
	 * @param y
	 *            coordinate of a cell
	 * @param flag
	 *            set to either true or false
	 */
	public void setCellYCorrect(int y, boolean flag) {
		for (Cell c : cellList) {
			if (c.getY() == y) {
				c.setYIsCorrect(flag);
			}
		}
	}

	/**
	 * Check whether Region's cells are all filled
	 * 
	 * @return true if none of the Region's Cells have empty values
	 */
	public boolean isComplete() {
		for (Cell c : this.cellList) {
			// NOTE: comparison for empty string, eg. c.getValue().equals(""),
			// does not work
			if (c.getValue().isEmpty()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Find a Cell object in Region with given (x, y) position coordinates Used
	 * by SudokuPuzzle
	 * 
	 * @param x
	 *            Cell's x coordinate in the sudoku puzzle
	 * @param y
	 *            Cell's y coordinate in the sudoku puzzle
	 * @return the value of the cell if the cell is in this region; null
	 *         otherwise
	 */
	public String getCellValue(int x, int y) {
		String value = null;
		for (Cell cell : cellList) {
			if ((cell.getX() == x) && (cell.getY() == y)) {
				value = cell.getValue();
			}
		}
		return value;
	}

	/**
	 * Returns puzzle to initial game state (clear editable values in each cell
	 * in this region)
	 */
	public void resetRegion() {
		for (Cell cell : cellList) {
			cell.setValue("");
		}
	}

	/**
	 * Looks for a cell with given (x,y) coordinate within this region and
	 * updates its value
	 * 
	 * @param x
	 *            Cell's x coordinate in the sudoku puzzle
	 * @param y
	 *            Cell's y coordinate in the sudoku puzzle
	 * @param value
	 *            the value to set the cell's value to
	 */
	public void setCellValue(int x, int y, String value) {
		for (Cell c : cellList) {
			if ((c.getX() == x) && (c.getY() == y)) {
				c.setValue(value);
				break;
			}
		}
	}

	/**
	 * Write save data for each cell to a given BufferedWriter
	 * 
	 * @param writer
	 *            BufferedWriter object for save data
	 */
	public void writeSaveData(BufferedWriter writer) {
		for (Cell cell : cellList) {
			if (cell.isEditable() && !cell.getValue().isEmpty()) {
				try {
					writer.write(cell.saveString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}