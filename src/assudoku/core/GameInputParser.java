package assudoku.core;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * GameInputFileParser class - reads puzzle structure from file input and
 * constructs objects to represent the sudoku puzzle
 * 
 * @author tvho795, emilys, kler763
 * 
 */
class GameInputParser {

	private String[] validValueList;
	private List<Region> regionList;
	private CellMasterList cellMasterList;

	/**
	 * Constructor: Opens, reads and interprets file lines from given file for a
	 * new sudoku game
	 * 
	 * @param sudokuFile
	 * @param fileName
	 *            name of file containing sudoku game
	 */
	public GameInputParser(SudokuPuzzle puzzle, String sudokuFile) {
		this.regionList = new ArrayList<Region>();
		this.cellMasterList = new CellMasterList();
		Region newRegion = null;

		try {
			Scanner sc = new Scanner(new FileReader(sudokuFile));

			// first read validValueList and store;
			String[] inputFields = sc.nextLine().split(" ");
			// initialise string array size
			this.validValueList = new String[inputFields.length - 1];
			for (int i = 1; i < inputFields.length; i++) {
				this.validValueList[i - 1] = inputFields[i];
			}

			int regionId = 0;
			while (sc.hasNextLine()) {
				inputFields = sc.nextLine().split(" ");

				if (inputFields[0].equals("Region")) {
					// create a new region and add to list of regions
					newRegion = new Region(puzzle, regionId,
							this.validValueList, this.cellMasterList);
					this.regionList.add(newRegion);
					regionId++;
				} else {
					// tell region to add a cell using the inputFields
					int x = Integer.parseInt(inputFields[0]);
					int y = Integer.parseInt(inputFields[1]);
					String value = "";
					boolean isEditable = true;

					// if a value is given in the input file
					if (inputFields.length == 3) {
						value = inputFields[2];
						isEditable = false;
					}

					newRegion.addCell(x, y, value, isEditable);
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
		}
	}

	/**
	 * Return a list of valid values accepted for this game
	 * 
	 * @return String array with the list of valid values for this game
	 */
	public String[] getValidValueList() {
		return this.validValueList;
	}

	/**
	 * Return the constructed regionList that represent this puzzle
	 * 
	 * @return a List of Region objects that store the puzzle's cells
	 */
	public List<Region> getRegionList() {
		return this.regionList;
	}

}
