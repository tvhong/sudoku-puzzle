package assudoku.core;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Class for generating a filepath for a sudoku game input file, based on user's
 * selected preferences
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class FileNameGenerator {

	public static final int LEVEL_EASY = 0;
	public static final int LEVEL_MEDIUM = 1;
	public static final int LEVEL_HARD = 2;
	public static final int LEVEL_JIGSAW = 3;

	private static final String FINISHED_PUZZLES = "finishedPuzzles";

	/**
	 * This haven't finished
	 * 
	 * @param size
	 *            the size of the puzzle, 3 for standard sudoku
	 * @param level
	 *            difficulty of the Puzzle. Use the public variables to do it.
	 *            NOTE: this currently just take 1 file for each level. Still
	 *            working on it
	 */
	public String getFileName(int size, int level) {
		StringBuilder pathFolder = new StringBuilder();
		pathFolder.append("presetInput/");
		pathFolder.append("size" + size + "/");
		if (level == LEVEL_EASY) {
			pathFolder.append("easy/");
		} else if (level == LEVEL_MEDIUM) {
			pathFolder.append("medium/");
		} else if (level == LEVEL_HARD) {
			pathFolder.append("hard/");
		} else if (level == LEVEL_JIGSAW) {
			pathFolder.append("jigsaw/");
		}

		// get a list of filename from the folder
		File folder = new File(pathFolder.toString());
		String[] puzzleNames = folder.list(new FilenameFilter() {
			public boolean accept(File folder, String fname) {
				return fname.endsWith(".puz");
			}
		});

		// read a file called finishedPuzzles
		String finishedPuzzlesPath = System.getProperty("user.dir") + "/"
				+ pathFolder.toString() + FileNameGenerator.FINISHED_PUZZLES;
		File finishedPuzzlesFile = new File(finishedPuzzlesPath);
		if (!finishedPuzzlesFile.exists()) {
			try {
				finishedPuzzlesFile.createNewFile();
			} catch (IOException e) {
				System.out.println("Problem creating file");
				e.printStackTrace();
			}
		}

		ArrayList<String> finishedPuzzles = new ArrayList<String>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(
					finishedPuzzlesPath));
			String line = br.readLine();

			while (line != null) {
				finishedPuzzles.add(line);
				line = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// randomly choose one file
		Random randomGenerator = new Random();
		String chosenPuzzleName = puzzleNames[randomGenerator
				.nextInt(puzzleNames.length)];
		int attemptCnt = 0;

		// generate a random puzzleName until we get a new game
		while (finishedPuzzles.contains(chosenPuzzleName) && attemptCnt < 20) {
			chosenPuzzleName = puzzleNames[randomGenerator
					.nextInt(puzzleNames.length)];
			attemptCnt++;
		}

		pathFolder.append(chosenPuzzleName);
		return pathFolder.toString();
	}
}
