package assudoku.core;
import java.util.ArrayList;
import java.util.List;


/**
 * Maintain a master list of Cells occurring in the puzzle - Only one cell
 * object may exist for any given (x,y) in the puzzle any subsequent Regions
 * that contain the same cell shall contain a reference to the original Cell
 * object, rather than creating another new Cell object with the same (x,y)
 * position, which would create conflict in puzzles where Regions share Cells (eg.
 * overlapping Regions).
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class CellMasterList {
	private List<Cell> cellMasterList;

	/**
	 * Constructor - constructs a new ArrayList of Cell objects
	 */
	public CellMasterList() {
		this.cellMasterList = new ArrayList<Cell>();
	}

	/**
	 * Check if a cell of given (x, y) already exists in the puzzle. If true,
	 * then return the given cell, else return null
	 * 
	 * @param x
	 *            coordinate of cell
	 * @param y
	 *            coordinate of cell
	 * @return found Cell if it exists, else null
	 */
	public Cell findExistingCell(int x, int y) {
		for (Cell cell : cellMasterList) {
			if (cell.getX() == x && cell.getY() == y) {
				return cell;
			}
		}
		return null;
	}

	/**
	 * Add Cell object to this cellMasterList
	 * 
	 * @param cell
	 *            to be added to the master list
	 */
	public void add(Cell cell) {
		if (!cellMasterList.contains(cell)) {
			cellMasterList.add(cell);
		}
	}
}
