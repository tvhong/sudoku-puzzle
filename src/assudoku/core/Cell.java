package assudoku.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Cell class - stores data related to a cell in a sudoku game, such as (x,y)
 * coordinates relative to a grid layout, a cell value, and flags for checking
 * its correctness according to game logic, etc.
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class Cell {

	private final int x, y;
	private String value;
	private final boolean isEditable;
	private List<Region> parentRegionList;
	private boolean regionIsCorrect, xIsCorrect, yIsCorrect;

	/**
	 * Cell class constructor.
	 * 
	 * @param x
	 *            the x position of the cell.
	 * @param y
	 *            the y position of the cell.
	 * @param value
	 *            the String value for this cell.
	 * @param isEditable
	 *            the editable flag of the cell.
	 */
	public Cell(int x, int y, String value, boolean isEditable) {
		this.x = x;
		this.y = y;
		this.value = value;
		this.isEditable = isEditable;

		// at the initialisation of the game, all cells are correct
		this.regionIsCorrect = this.xIsCorrect = this.yIsCorrect = true;
		this.parentRegionList = new ArrayList<Region>();
	}

	/**
	 * Adds a reference to a parent region to the list of parent regions This
	 * means a cell could belong to several regions (in the case of harder
	 * sudoku puzzles which have overlapping segments)
	 * 
	 * @param parentRegion
	 *            a Region that this cell occurs in
	 */
	public void addParentRegion(Region parentRegion) {
		if (!this.parentRegionList.contains(parentRegion)) {
			this.parentRegionList.add(parentRegion);
		}
	}

	/**
	 * Get a list of this cell's parent regions
	 * 
	 * @return list of Regions that this cell occurs in
	 */
	// used in RenderCell - checks for size > 1 in order to paint a 3rd neutral
	// color
	// for overlapping cells/regions - such as in very hard sudoku puzzles.
	// Currently we do not support such puzzles, but we'd like to expand to that
	public List<Region> getParentRegion() {
		return this.parentRegionList;
	}

	/**
	 * Get the first parentRegion that this cell is in; in the case of puzzles
	 * where every cell only occurs in one region, it will always be the first
	 * element in the parentRegionList
	 * 
	 * @return first Region object that this cell occurs in
	 */
	public Region peekParentRegion() {
		return this.parentRegionList.get(0);
	}

	/**
	 * Get cell's x coordinate.
	 * 
	 * @return x coordinate of cell
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Get cell's y coordinate.
	 * 
	 * @return y coordinate of cell
	 */
	public int getY() {
		return this.y;
	}

	/**
	 * Get cell value
	 * 
	 * @return the String representation of the value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Set cell value
	 * 
	 * @param value
	 *            String to set this cell's value to
	 */
	public void setValue(String value) {
		if (this.isEditable) {
			this.value = value;
		}
	}

	/**
	 * Check whether cell value should can be edited by the GUI
	 * 
	 * @return true if cell value can be edited
	 */
	public boolean isEditable() {
		return this.isEditable;
	}

	/**
	 * Produces a representation of the cell information for storing in a save
	 * game file that can be interpreted later when loading a game from that
	 * same file
	 * 
	 * @return string representation of cell value in the format of "x y value"
	 */
	public String saveString() {
		return (this.getX() + " " + this.getY() + " " + this.getValue() + "\n");
	}

	/**
	 * Check whether the cell value meets the game logic for a valid solution To
	 * be used by the GUI for error highlighting of incorrect cell groups
	 * 
	 * @return true if cell meets the game logic for a valid solution
	 */
	public boolean isCorrect() {
		return (this.regionIsCorrect && this.xIsCorrect && this.yIsCorrect);
	}

	// Methods for setting correctness of this Cell based on game logic
	/**
	 * Set whether cell is correct based on whether its Region is correct
	 * 
	 * @param flag
	 *            set to either true or false
	 */
	public void setRegionIsCorrect(boolean flag) {
		this.regionIsCorrect = flag;
	}

	/**
	 * Set whether a cell is correct based on whether all other cells with the
	 * same x coordinate are correct
	 * 
	 * @param flag
	 *            set to either true or false
	 */
	public void setXIsCorrect(boolean flag) {
		this.xIsCorrect = flag;
	}

	/**
	 * Set whether a cell is correct based on whether all other cells with the
	 * same y coordinate are correct
	 * 
	 * @param flag
	 *            set to either true or false
	 */
	public void setYIsCorrect(boolean flag) {
		this.yIsCorrect = flag;
	}

}
