package assudoku.core;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * SudokuPuzzle class: represents a sudoku puzzle consisting of n^2 regions for
 * a puzzle of size n
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class SudokuPuzzle {

	private final List<Region> regionList;
	private final String[] validValues;
	public String sudokuFile;
	public static final String SAVE_GAME_FILEPATH = "presetInput/saveGameFile.txt";

	/**
	 * Constructor
	 * 
	 * @param sudokuFile
	 *            String representing filename for a sudoku game input file
	 */
	public SudokuPuzzle(String sudokuFile) {
		this.sudokuFile = sudokuFile; // temporary for testing SaveGame
		GameInputParser parser = new GameInputParser(this, sudokuFile);
		this.regionList = parser.getRegionList();
		this.validValues = parser.getValidValueList();
	}

	/**
	 * Return list of Regions that make up this puzzle
	 * 
	 * @return List of Region objects
	 */
	public List<Region> getRegions() {
		return this.regionList;
	}

	/**
	 * Checks if a column of cells with coordinate x are unique
	 * 
	 * @param x
	 *            coordinate of cells
	 * @return true if group of cells with given x coordinate are valid
	 */
	public boolean isColXCorrect(int x) {
		List<String> valueList = new ArrayList<String>();
		// gets cell values from all cells with x coordinate from all the
		// regions
		// puts them in a String (array of char, essentially)

		// NOTE: This would not work in its current incarnation if we were
		// playing
		// a game where the valid values are more than 1 character in length
		for (int i = 0; i < validValues.length; i++) {
			String s = getCellValue(x, i);
			valueList.add(s);
		}
		// checks uniqueness of valueList (either true or false) and sets
		// a correctness flag in all cells with this x coordinate
		boolean result = checkUniqueness(valueList);
		setCellXCorrect(x, result);
		return result;
	}

	/**
	 * Checks if a row of cells with coordinate y are unique
	 * 
	 * @param y
	 *            referring to a y coordinate for a group of cells
	 * @return true if the row of cells with given y coordinate are valid for
	 *         the game logic
	 */
	public boolean isRowYCorrect(int y) {
		List<String> valueList = new ArrayList<String>();
		// gets cell values from all cells with y coordinate from all the
		// regions
		// puts them in a String (array of char, essentially)

		// NOTE: This would not work in its current incarnation if we were
		// playing
		// a game where the valid values are more than 1 character in length

		for (int i = 0; i < validValues.length; i++) {
			String s = getCellValue(i, y);
			valueList.add(s);
		}
		// checks uniqueness of valueList (either true or false) and sets
		// a correctness flag in all cells with this y coordinate
		boolean result = checkUniqueness(valueList);
		setCellYCorrect(y, result);
		return result;
	}

	/**
	 * Sets cells of given x coordinate with a flag to indicate if the cell is
	 * correct based on column uniqueness
	 * 
	 * @param x
	 *            Cell's x coordinate in the sudoku puzzle
	 * @param flag
	 *            sets the cell as being true or false based on its x coordinate
	 */
	public void setCellXCorrect(int x, boolean flag) {
		for (Region r : regionList) {
			r.setCellXCorrect(x, flag);
		}
	}

	/**
	 * Sets cells of given y coordinate with a flag to indicate if the cell is
	 * correct based on row uniqueness
	 * 
	 * @param y
	 *            Cell's y coordinate in the sudoku puzzle
	 * @param flag
	 *            sets the cell as being true or false based on its y coordinate
	 */
	public void setCellYCorrect(int y, boolean flag) {
		for (Region r : regionList) {
			r.setCellYCorrect(y, flag);
		}
	}

	/**
	 * Checks if a list of given cell values are unique and valid
	 * 
	 * @param valueList
	 * @return
	 */
	private boolean checkUniqueness(List<String> valueList) {
		boolean result = true;
		// flags[i]==1 <=> the value is visited
		boolean[] flags = new boolean[validValues.length];
		for (String s : valueList) {
			if (s.equals("")) { // don't check blank cells
				continue;
			}
			boolean isValidValue = false; // check if this cell has a valid
			// value
			// for every cell, check if that cell's value is in the validValues
			// list or not.
			for (int i = 0; (i < validValues.length) && result; i++) {
				if (validValues[i].equals(s)) {
					if (flags[i] == true) { // if the value is already visited
						result = false;
					}
					flags[i] = true;
					isValidValue = true;
				}
			}
			result = result && isValidValue; // make sure that this cell's value
			// is valid
		}
		return result;
	}

	/**
	 * Return value of cell of given (x, y)
	 * 
	 * @param x
	 *            Cell's x coordinates in the sudoku
	 * @param y
	 *            Cell's y coordinate in the sudoku
	 * @return string representing cell value
	 */
	public String getCellValue(int x, int y) {
		// this is essentially searching through all regions and cells
		// can try to optimise implementation later, first we'll see if this
		// works
		String value = null;
		for (Region r : this.regionList) {
			value = r.getCellValue(x, y);
			if (value != null) { // found the value
				return value;
			}
		}
		return value;
	}

	/**
	 * Checks whether every cell in the puzzle has a non-empty value
	 * 
	 * @return true if puzzle does not have any empty cells, otherwise false
	 */
	public boolean isComplete() {
		for (Region r : this.regionList) {
			if (!r.isComplete()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks whether all rows and columns are valid
	 * 
	 * @return true if rows and columns in the puzzle are valid for the game
	 *         logic
	 */
	public boolean rowsColumnsCorrect() {
		for (int n = 0; n < validValues.length; n++) {
			if (!isColXCorrect(n) || !isRowYCorrect(n)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks whether regions are valid
	 * 
	 * @return true if all of the regions in the puzzle are valid for the game
	 *         logic
	 */
	public boolean regionsCorrect() {
		// need to iterate through all regions to ensure all cell.regionCorrect
		// flags are set
		boolean correct = true;
		for (Region r : regionList) {
			if (!r.isCorrect()) {
				correct = false;
			}
		}
		return correct;
	}

	/**
	 * Checks whether the game has been solved (ie. all cells are non-empty and
	 * valid)
	 * 
	 * @return true if the puzzle has been solved, otherwise false
	 */
	public boolean isSolved() {
		return (rowsColumnsCorrect() && regionsCorrect() && isComplete());
	}

	/**
	 * Returns puzzle to initial game state (clear editable values in each
	 * Region in the puzzle)
	 */
	public void resetPuzzle() {
		for (Region region : regionList) {
			region.resetRegion();
		}
		regionsCorrect();
		rowsColumnsCorrect();
	}

	/**
	 * Inform regions to set a cell with given x, y position in the puzzle with
	 * a new value
	 * 
	 * @param x
	 *            Cell's x coordinate in the sudoku puzzle
	 * @param y
	 *            Cell's y coordinate in the sudoku puzzle
	 * @param newCellValStr
	 *            new String value to set the cell's value to
	 */
	public void setCellValue(int x, int y, String newCellValStr) {
		// this is essentially searching through all regions and cells
		// can try to optimise implementation later, first we'll see if this
		// works
		for (Region r : this.regionList) {
			r.setCellValue(x, y, newCellValStr);
		}
	}

	/**
	 * Save current game to file; including a reference to the original game
	 * input file and any subsequent values entered into the puzzle
	 */
	public void saveGame() {
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(SAVE_GAME_FILEPATH));
			writer.write(this.sudokuFile + "\n");

			// tell sudokuPuzzle to write its save date to the given
			// BufferedWriter
			writeSaveData(writer);

			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write save data for each region to a given BufferedWriter
	 * 
	 * @param writer
	 *            BufferedWriter object for save data
	 */
	public void writeSaveData(BufferedWriter writer) {
		for (Region region : regionList) {
			region.writeSaveData(writer);
		}
	}

}