package assudoku.gui;
import javax.swing.*;

/**
 * An interface for any screen to display on the main frame
 * 
 * @author tvho795, emilys, kler763
 */
public interface ScreenInterface {

	/**
	 * Creates components to displayed on the screen
	 */
	public void createMenu();

	/**
	 * Returns the panel to add to the 'mainFrame' to display it on the screen
	 * 
	 * @return JPanel
	 */
	public JPanel getPanel();
}
