package assudoku.gui;
import javax.swing.text.*;

/**
 * JTextFieldLimit implements method to limit cell input to 1 character
 * @author kevin
 * 
 */
public class JTextFieldLimit extends PlainDocument {
	private int limit;

	public JTextFieldLimit(int limit) {
		super();
		this.limit = limit;
	}

	public void insertString(int offset, String str, AttributeSet attr)
			throws BadLocationException {
		if (str == null)
			return;

		if ((getLength() + str.length()) <= limit) {
			super.insertString(offset, str, attr);
		}
	}
}