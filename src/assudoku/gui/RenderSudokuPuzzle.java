package assudoku.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import assudoku.core.Region;
import assudoku.core.SudokuPuzzle;

/**
 * RenderSudokuPuzzle class renders elements of a SudokuPuzzle object for
 * displaying to the GUI
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class RenderSudokuPuzzle implements CellValueListener {

	public final SudokuPuzzle sudokuPuzzle;
	private List<RenderRegion> renderRegionList;

	private SudokuLayout colourScheme;
	private boolean errorHighlighting;

	/**
	 * Constructor - initialises new RenderRegion objects for each Region in the
	 * SudokuPuzzle and pushes them to a list of RenderRegion objects kept by
	 * this class
	 * 
	 * @param sudokuPuzzle
	 *            puzzle object that we are creating render objects for
	 * @param sudokuPanel
	 *            pane that we are adding rendered components to
	 * @param colourScheme
	 *            current SudokuLayout theme that we are using to set appearance
	 *            for components
	 * @param errorHighlighting
	 *            flag to tell us whether or not to set different appearance for
	 *            incorrect cells
	 */
	public RenderSudokuPuzzle(SudokuPuzzle sudokuPuzzle, JPanel sudokuPanel,
			SudokuLayout colourScheme, boolean errorHighlighting) {
		this.sudokuPuzzle = sudokuPuzzle;
		this.colourScheme = colourScheme;
		this.errorHighlighting = errorHighlighting;
		this.renderRegionList = new ArrayList<RenderRegion>();

		// create RenderRegion objects for this puzzle's Regions
		List<Region> regionList = this.sudokuPuzzle.getRegions();
		RenderCellMasterList renderCellMasterList = new RenderCellMasterList();
		for (Region region : regionList) {
			RenderRegion renderRegion = new RenderRegion(this, region,
					sudokuPanel, renderCellMasterList);
			this.renderRegionList.add(renderRegion);
		}
		draw();
	}

	/**
	 * Redraw all the RenderRegion objects in this RenderSudokuPuzzle
	 */
	public void draw() {
		for (RenderRegion renderRegion : renderRegionList) {
			renderRegion.draw(this.colourScheme, this.errorHighlighting);
		}
	}

	/**
	 * Set to given SudokuLayout theme then redraw GUI to reflect the change
	 * 
	 * @param colourScheme
	 *            a SudokuLayout that contains a theme for displaying GUI
	 *            components
	 */
	public void setColourScheme(SudokuLayout colourScheme) {
		this.colourScheme = colourScheme;
		draw();
	}

	/**
	 * Turn error highlighting on or off (depending on flag) then redraw GUI to
	 * reflect the change
	 * 
	 * @param flag
	 */
	public void setErrorHighlighting(boolean flag) {
		this.errorHighlighting = flag;
		draw();
	}

	/**
	 * Method that performs whatever required actions for when a guiCell value
	 * has been updated/removed In this implementation, if error highlighting is
	 * turn on then it redraws the interface with every change that occurs in
	 * the guiCell's text. if error highlighting is turned off, it does not
	 * redraw
	 */
	@Override
	public void performOnCellValueChange(int x, int y) {
		this.sudokuPuzzle.regionsCorrect();
		this.sudokuPuzzle.isColXCorrect(x);
		this.sudokuPuzzle.isRowYCorrect(y);
		if (this.errorHighlighting) {
			this.draw(); // redraws the puzzle
		}
	}

	/**
	 * Reset each Region in this puzzle to starting state
	 */
	public void reset() {
		for (RenderRegion renderRegion : renderRegionList) {
			renderRegion.reset();
		}
	}

}
