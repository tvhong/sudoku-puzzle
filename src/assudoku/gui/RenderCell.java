package assudoku.gui;

import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import assudoku.core.Cell;

/**
 * RenderCell class renders elements of a Cell object for displaying to the GUI
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class RenderCell {

	private final Cell cell;
	public final JTextField guiCell;
	private List<RenderRegion> parentRenderRegionList;
	private List<CellValueListener> cellListeners;

	/**
	 * Constructor - constructs new RenderCell object that will contain a
	 * component and add it to our GUI's JPanel for displaying
	 * 
	 * @param cell
	 *            Cell object that is associated with this RenderCell object
	 * @param sudokuPanel
	 *            pane that we are adding rendered components to
	 */
	public RenderCell(final Cell cell, JPanel sudokuPanel) {
		this.cell = cell;

		this.guiCell = new JTextField();
		this.guiCell.setDocument(new JTextFieldLimit(1));
		this.guiCell.setEditable(cell.isEditable());
		this.guiCell.setText(cell.getValue());
		this.guiCell.setHorizontalAlignment(JTextField.CENTER);
		sudokuPanel.add(guiCell, setConstraints());

		this.parentRenderRegionList = new ArrayList<RenderRegion>();
		this.cellListeners = new ArrayList<CellValueListener>();

		// Document Listener - performs actions when actions occur in the text
		// field
		guiCell.getDocument().addDocumentListener(new DocumentListener() {

			// is called when a new value has been entered into the document
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				cellUpdate(cell.getX(), cell.getY());
			}

			// is called when the document text has been deleted
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				cellUpdate(cell.getX(), cell.getY());
			}

			// is called when a change has been made to existing text in the
			// document
			@Override
			public void changedUpdate(DocumentEvent e) {
				// nothing really needed here
				cellUpdate(cell.getX(), cell.getY());
			}

		});
	}

	/**
	 * Set GridBagConstraints
	 * 
	 * @return GridBagConstraints object containing new constraints settings
	 */
	public GridBagConstraints setConstraints() {
		GridBagConstraints con = new GridBagConstraints();
		con.ipadx = 30;
		con.ipady = 10;
		con.fill = GridBagConstraints.BOTH;
		con.gridx = this.cell.getX();
		con.gridy = this.cell.getY();
		return con;
	}

	/**
	 * Return the Cell object that is associated with this RenderCell
	 * 
	 * @return Cell object associated with this RenderCell
	 */
	public Cell getCell() {
		return this.cell;
	}

	/**
	 * If guiCell text is editable, update the guiCell text to the current Cell
	 * value Used when resetting the puzzle and redisplaying to Gui
	 */
	public void updateGuiCellText() {
		if (this.guiCell.isEditable()) {
			this.guiCell.setText(this.cell.getValue());
		}
	}

	/**
	 * Add reference to a RenderRegion object that wants to know about this
	 * RenderCell
	 * 
	 * @param parentRenderRegion
	 */
	public void addParentRenderRegion(RenderRegion parentRenderRegion) {
		if (!this.parentRenderRegionList.contains(parentRenderRegion)) {
			this.parentRenderRegionList.add(parentRenderRegion);
		}
	}

	/**
	 * Get the first parentRenderRegion that this cell is in; in the case of
	 * puzzles where every cell only occurs in one region, it will always be the
	 * first element in the parentRenderRegionList
	 * 
	 * @return first RenderRegion object that this cell occurs in
	 */
	public RenderRegion peekParentRenderRegion() {
		return this.parentRenderRegionList.get(0);
	}

	/**
	 * Add a reference to a CellValueListener class
	 * 
	 * @param listener
	 *            a class that implements CellValueListener that is interested
	 *            in updates to this guiCell
	 */
	public void addCellListener(CellValueListener listener) {
		if (!this.cellListeners.contains(listener)) {
			this.cellListeners.add(listener);
		}
	}

	/**
	 * Method called when guiCell text has been changed or removed (using
	 * DocumentListener)
	 * 
	 * @param x
	 *            coordinate of this cell
	 * @param y
	 *            coordinate of this cell
	 */
	protected void cellUpdate(int x, int y) {
		// first update the associated Cell object's value
		cell.setValue(guiCell.getText());

		// then for every interested CellValueListener, tell them to call their
		// performOnCellValueChange() method; which is whatever they need to do
		// when a cell's been changed
		for (CellValueListener listener : this.cellListeners) {
			listener.performOnCellValueChange(x, y);
		}
	}

	/**
	 * Re-render the text field with given appearance settings
	 * 
	 * @param colourScheme
	 */
	public void draw(SudokuLayout colourScheme, boolean errorHighlighting,
			int colourId) {
		// set the guiCell appearance to the default color themes
		this.guiCell.setFont(colourScheme.getFont());
		this.guiCell.setForeground(colourScheme.getFontColor(this.cell
				.isEditable()));
		this.guiCell.setBackground(colourScheme.getGridColor(colourId));

		// if cell belongs to more than 1 region AND is valid then paint it a
		// color used to indicate overlapping regions
		if (cell.getParentRegion().size() > 1 && cell.isCorrect()) {
			guiCell.setBackground(colourScheme
					.getGridColor(SudokuLayout.OVERLAP));
		}

		// if cell is incorrect and errorHighlighting is turned on, then paint
		// it the color specified for errors
		if (!cell.isCorrect() && errorHighlighting) {
			guiCell.setBackground(colourScheme.getGridColor(SudokuLayout.ERROR));
		}
	}

}
