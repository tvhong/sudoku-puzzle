package assudoku.gui;
import java.awt.event.*;
import javax.swing.*;

import assudoku.theme.FiveByFiveColors;
import assudoku.theme.OblivionColors;
import assudoku.theme.PapermanColors;
import assudoku.theme.RainbowColors;
import assudoku.theme.RayPickColors;

/**
 * A class which handles the sudoku menu bar. The Sudoku menu bar is the bar in
 * the top left hand corner of the frame.
 * 
 * @author tvho795, emilys, kler763
 */
public class SudokuMenuBar implements ActionListener {

	private Screen parentScreen;
	public JMenuBar menuBar;
	public JMenu fileMenu, optionsMenu, layoutSubMenu;

	private JMenuItem itemNewGame;
	private JMenuItem itemLoadGame;
	private JMenuItem itemQuitGame;
	private JMenuItem itemReturnToMenu;

	private JCheckBoxMenuItem optionErrorHighlight;
	private JMenuItem defaultTheme, rainbowTheme, oblivionTheme,
			fivebyfiveTheme, rayTheme;

	/**
	 * The menu bar constructor, adds all components to the menu bar and their
	 * functionalities
	 * 
	 * @param mainFrame
	 *            a reference to the main screen's frame, so the menu bar can be
	 *            added
	 * @param parentScreen
	 *            A reference to the screen, so actions can be applied
	 */
	public SudokuMenuBar(JFrame mainFrame, Screen parentScreen) {
		this.menuBar = new JMenuBar();
		addMenuComponents();
		mainFrame.setJMenuBar(menuBar);
		this.parentScreen = parentScreen;
	}

	/**
	 * Adding components and drop-down items to the menu bar, for example 'File'
	 * is a component 'New Game' & 'Save Game' are its drop-down items
	 */
	private void addMenuComponents() {

		// intialising bar components
		this.fileMenu = new JMenu("File");
		this.optionsMenu = new JMenu("Options");

		// adding Bar components
		menuBar.add(fileMenu);
		menuBar.add(optionsMenu);

		// initialising 'File' dropdown components
		itemNewGame = new JMenuItem("New Game");
		itemLoadGame = new JMenuItem("Load Game");
		itemReturnToMenu = new JMenuItem("Back to Main Menu");
		itemQuitGame = new JMenuItem("Quit");
		itemQuitGame = new JMenuItem("Quit");

		// adding 'File' dropdown components
		fileMenu.add(itemNewGame);
		fileMenu.add(itemLoadGame);
		fileMenu.add(itemReturnToMenu);
		fileMenu.add(itemQuitGame);

		// initialising 'Option' dropdown components
		optionErrorHighlight = new JCheckBoxMenuItem("Error Highlighting");

		this.layoutSubMenu = new JMenu("Change colour scheme");
		defaultTheme = new JMenuItem("Paperman");
		rainbowTheme = new JMenuItem("Rainbow Pastels");
		oblivionTheme = new JMenuItem("Oblivion");
		fivebyfiveTheme = new JMenuItem("Five By Five");
		rayTheme = new JMenuItem("Ray's Fave Theme");
		layoutSubMenu.add(defaultTheme);
		layoutSubMenu.add(rainbowTheme);
		layoutSubMenu.add(oblivionTheme);
		layoutSubMenu.add(fivebyfiveTheme);
		layoutSubMenu.add(rayTheme);

		// adding 'Option' dropdown components
		optionsMenu.add(optionErrorHighlight);
		optionsMenu.add(layoutSubMenu);

		// adding action listeners();
		itemNewGame.addActionListener(this);
		itemLoadGame.addActionListener(this);
		itemReturnToMenu.addActionListener(this);
		itemQuitGame.addActionListener(this);
		optionErrorHighlight.addActionListener(this);
		defaultTheme.addActionListener(this);
		rainbowTheme.addActionListener(this);
		oblivionTheme.addActionListener(this);
		fivebyfiveTheme.addActionListener(this);
		rayTheme.addActionListener(this);
	}

	/**
	 * Actions for each drop-down item
	 */
	public void actionPerformed(ActionEvent e) {
		// Actions for 'File' drop-down items
		if (e.getSource() == itemNewGame) {
			parentScreen.goToDiffSet();
		} else if (e.getSource() == itemLoadGame) {
			// Add what Load game does here
			parentScreen.loadSaveGame();
		} else if (e.getSource() == itemReturnToMenu) {
			parentScreen.goToMainMenu();
		} else if (e.getSource() == itemQuitGame) {
			parentScreen.quit();
		} else {
			if (e.getSource() == optionErrorHighlight) {
				// toggle error highlighting mode
				if (parentScreen.errorHighlighting) {
					parentScreen.errorHighlighting = false;
				} else {
					parentScreen.errorHighlighting = true;
				}
				// apply different color themes
			} else if (e.getSource() == defaultTheme) {
				parentScreen.setColorScheme(new PapermanColors());
			} else if (e.getSource() == rainbowTheme) {
				parentScreen.setColorScheme(new RainbowColors());
			} else if (e.getSource() == oblivionTheme) {
				parentScreen.setColorScheme(new OblivionColors());
			} else if (e.getSource() == fivebyfiveTheme) {
				parentScreen.setColorScheme(new FiveByFiveColors());
			} else if (e.getSource() == rayTheme) {
				parentScreen.setColorScheme(new RayPickColors());
			}
			// refresh appearance of sudoku GUI after updating appearance
			parentScreen.refreshSudokuAppearance();
		}
	}

}
