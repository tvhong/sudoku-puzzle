package assudoku.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import assudoku.core.Cell;
import assudoku.core.Region;

/**
 * RenderRegion class renders elements of a Region object for displaying to the
 * GUI
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class RenderRegion {

	private final Region region;
	public final RenderSudokuPuzzle parentRenderSudokuPuzzle;
	private List<RenderCell> renderCellList;

	private final RenderCellMasterList renderCellMasterList;

	/**
	 * Constructor - initialises new RenderCell objects for each Region in the
	 * SudokuPuzzle and pushes them to a list of RenderCell objects kept by this
	 * class
	 * 
	 * @param parentRenderSudokuPuzzle
	 *            RenderSudokuPuzzle that contains this RenderRegion
	 * @param region
	 *            Region object that we are creating RenderCell objects for
	 * @param sudokuPane
	 *            pane that we are adding rendered components to
	 */
	public RenderRegion(RenderSudokuPuzzle parentRenderSudokuPuzzle,
			Region region, JPanel sudokuPane,
			RenderCellMasterList renderCellMasterList) {
		this.region = region;
		this.parentRenderSudokuPuzzle = parentRenderSudokuPuzzle;
		this.renderCellList = new ArrayList<RenderCell>();
		this.renderCellMasterList = renderCellMasterList;

		addRenderCells(sudokuPane);
	}

	/**
	 * Create RenderCell objects for the region's cells and to this RenderRegion
	 */
	public void addRenderCells(JPanel sudokuPane) {
		List<Cell> cellList = this.region.getCellList();
		for (Cell cell : cellList) {
			// check if a RenderCell object already exists in RenderSudokuPuzzle
			// for given cell
			RenderCell renderCell = this.renderCellMasterList
					.findExistingCell(cell);
			// if it doesn't exist, first create a new cell and add it to
			// cellMasterList
			if (renderCell == null) {
				renderCell = new RenderCell(cell, sudokuPane);
				this.renderCellMasterList.add(renderCell);
			}

			// add the cell to this region's list of cells
			// add a reference to this region to in the cell
			this.renderCellList.add(renderCell);
			renderCell.addParentRenderRegion(this);

			// add references to CellValueListener classes that are concerned
			// with changes to the guiCell value; they must be in the order of
			// sudokuPuzzle first, then renderSudokuPuzzle (to ensure the puzzle
			// is checked for correctness and flags are set in the cells before
			// redrawing the whole puzzle to GUI
			renderCell.addCellListener(this.parentRenderSudokuPuzzle);
		}
	}

	/**
	 * Redraw all the RenderCell objects in this RenderRegion
	 * 
	 * @param colourScheme
	 *            SudokuLayout theme to use for redrawing
	 * @param errorHighlighting
	 *            flag for telling RenderCell whether to use error highlighting
	 *            or not
	 */
	public void draw(SudokuLayout colourScheme, boolean errorHighlighting) {
		for (RenderCell renderCell : renderCellList) {
			renderCell.draw(colourScheme, errorHighlighting,
					this.region.getRegionId());
		}
	}

	/**
	 * Reset each cell in this region to starting state
	 */
	public void reset() {
		for (RenderCell renderCell : renderCellList) {
			renderCell.updateGuiCellText();
		}
	}
}
