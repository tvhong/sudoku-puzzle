package assudoku.gui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.*;

import assudoku.core.SudokuPuzzle;

/**
 * The sudoku Graphical user interface. Initializes the sudoku sudokuPuzzle from
 * text which acts as the back end of the sudoku. Also calls a series of
 * renderers to draw the GUI.
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class SudokuGui implements ActionListener {

	private final SudokuPuzzle sudokuPuzzle;
	private final JPanel cellsPanel;
	private JPanel sudokuGuiPanel;
	private final String sudokuFile;
	private RenderSudokuPuzzle renderSudokuPuzzle;
	private SudokuLayout colorScheme;
	private boolean errorHighlighting;

	/**
	 * Constructor - takes a SudokuPuzzle object and initialises GUI frame
	 * components
	 * 
	 * @param sudokuPuzzle
	 *            SudokuPuzzle object that represents the game structure/data
	 */
	public SudokuGui(String sudokuFile, SudokuPuzzle sudokuPuzzle,
			SudokuLayout colorScheme, boolean errorHighlighting) {
		this.sudokuPuzzle = sudokuPuzzle;
		this.colorScheme = colorScheme;
		this.errorHighlighting = errorHighlighting;
		this.cellsPanel = new JPanel();
		this.sudokuFile = sudokuFile;
		constructGui();
	}

	/**
	 * Render (draw/display) components of the sudoku puzzle
	 */
	private void constructGui() {
		// A sudokuGuiPanel to contain all other sub panels
		sudokuGuiPanel = new JPanel();
		sudokuGuiPanel.setLayout(new BoxLayout(sudokuGuiPanel,
				BoxLayout.PAGE_AXIS));

		// Set layout for cellsPanel to store the sudoku puzzle GUI text fields
		// Parse cellsPanel when rendering the puzzle so that constructed
		// components are added to the panel
		this.cellsPanel.setLayout(new GridBagLayout());
		this.renderSudokuPuzzle = new RenderSudokuPuzzle(this.sudokuPuzzle,
				this.cellsPanel, this.colorScheme, this.errorHighlighting);

		// A buttonPanel to present buttons for use during gameplay
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());

		// A reset button to reset the game to its initial empty state
		JButton resetButton = new JButton("Restart this puzzle");
		resetButton.setActionCommand("reset");
		resetButton.addActionListener(this);

		// A check button to check if the current puzzle state is a correct
		// solution
		JButton checkSolutionButton = new JButton("Check my answer");
		checkSolutionButton.setActionCommand("check");
		checkSolutionButton.addActionListener(this);

		JButton saveGameButton = new JButton("Save this game");
		saveGameButton.setActionCommand("save");
		saveGameButton.addActionListener(this);

		// Add buttons to the buttonPanel
		buttonPanel.add(resetButton);
		buttonPanel.add(checkSolutionButton);
		buttonPanel.add(saveGameButton);

		// Add sub-panels to the sudokuGuiPanel
		sudokuGuiPanel.add(this.cellsPanel);
		sudokuGuiPanel.add(buttonPanel);
	}

	/**
	 * On action - button presses or other actions
	 * 
	 * @param e
	 *            some event
	 */
	public void actionPerformed(ActionEvent e) {
		if ("reset".equals(e.getActionCommand())) {
			// resets game values to initial empty state
			resetGui();
		} else if ("check".equals(e.getActionCommand())) {
			// check if current puzzle state is correct solution
			// print appropriate message
			JFrame isSolvedFrame = new JFrame();
			if (this.sudokuPuzzle.isSolved()) {
				JOptionPane.showMessageDialog(isSolvedFrame,
						"Congratulations!! You've solved the puzzle!");
				String puzFile = sudokuFile.substring(sudokuFile
						.lastIndexOf('/') + 1);
				String folderPath = sudokuFile.substring(0,
						sudokuFile.lastIndexOf('/'));
				String finishedPuzzlePath = System.getProperty("user.dir")
						+ "/" + folderPath + "/finishedPuzzles";
				try {
					PrintWriter out = new PrintWriter(new BufferedWriter(
							new FileWriter(finishedPuzzlePath, true)));
					out.println(puzFile);
					out.close();
				} catch (IOException ioe) {
					System.err.println("IOException: " + ioe.getMessage());
				}
			} else {
				JOptionPane.showMessageDialog(isSolvedFrame,
						"This puzzle is still unsolved");
			}
		} else if ("save".equals(e.getActionCommand())) {
			File saveFile = new File(SudokuPuzzle.SAVE_GAME_FILEPATH);
			// safety check: if a saved game file already exists, ask user to
			// confirm before overwriting the saved game file
			if (saveFile.exists()) {
				int answer = JOptionPane
						.showConfirmDialog(
								sudokuGuiPanel,
								"A saved game file already exists.\nDo you wish to overwrite it with your current game?",
								"Save Game", JOptionPane.YES_NO_OPTION,
								JOptionPane.QUESTION_MESSAGE);
				if (answer == JOptionPane.YES_OPTION) {
					this.sudokuPuzzle.saveGame();
				}
			} else {
				this.sudokuPuzzle.saveGame();
			}
		}
	}

	/**
	 * Re-render (re-draw/display) sudoku puzzle with new settings (scheme or
	 * error highlighting)
	 * 
	 * @param colorScheme
	 *            SudokuLayout to apply when redrawing the puzzle
	 * @param errorHighlighting
	 *            true if puzzle cells are to be rendered with error
	 *            highlighting enabled
	 */
	public void drawGui(SudokuLayout colorScheme, boolean errorHighlighting) {
		this.errorHighlighting = errorHighlighting;
		this.colorScheme = colorScheme;
		this.renderSudokuPuzzle.setErrorHighlighting(this.errorHighlighting);
		this.renderSudokuPuzzle.setColourScheme(this.colorScheme);
		this.renderSudokuPuzzle.draw();
	}

	/**
	 * Tells GUI to reset puzzle and re-display
	 */
	public void resetGui() {
		this.sudokuPuzzle.resetPuzzle();
		this.renderSudokuPuzzle.reset();
	}

	/**
	 * Returns the JPanel containing the sudoku GUI components
	 * 
	 * @return JPanel object containing sudoku GUI components
	 */
	public JPanel getPanel() {
		return this.sudokuGuiPanel;
	}

	/**
	 * Load saved game data after loading initial game values from the original
	 * game file
	 * 
	 * @param savedCellData
	 *            ArrayList of Strings representing entered cell values
	 */
	public void loadSavedGameData(ArrayList<String> savedCellData) {
		for (String s : savedCellData) {
			String[] sTokens = s.split(" ");
			int x = Integer.parseInt(sTokens[0]);
			int y = Integer.parseInt(sTokens[1]);
			String value = sTokens[2];

			this.sudokuPuzzle.setCellValue(x, y, value);
		}
		// reset also involves setting sudoku GUI cells to update their values
		// in line with the sudoku game data, then redraws the GUI; this means a
		// saved game containing errors will display the error state correctly,
		// in the event that error highlighting mode is turned on
		this.renderSudokuPuzzle.reset();
	}

}