package assudoku.gui;
import java.util.ArrayList;
import java.util.List;

import assudoku.core.Cell;

/**
 * Maintain a master list of RenderCells occurring in the puzzle - Only one cell
 * object may exist for any given (x,y) in the puzzle any subsequent Regions
 * that contain the same cell shall contain a reference to the original Cell
 * object, rather than creating another new Cell object with the same (x,y)
 * position, which would create conflict in puzzles where Regions share Cells
 * (eg. overlapping Regions).
 * 
 * @author tvho795, emilys, kler763
 * 
 */

public class RenderCellMasterList {
	private List<RenderCell> renderCellMasterList;

	/**
	 * Constructor
	 */
	public RenderCellMasterList() {
		this.renderCellMasterList = new ArrayList<RenderCell>();
	}

	/**
	 * Check if a cell of given (x, y) already exists in the puzzle If true,
	 * then return the given cell, else return null
	 * 
	 * @return found Cell if it exists, else null
	 */
	public RenderCell findExistingCell(Cell cell) {
		for (RenderCell renderCell : renderCellMasterList) {
			if (renderCell.getCell().equals(cell)) {
				return renderCell;
			}
		}
		return null;
	}

	/**
	 * Add cell to cellMasterList
	 * 
	 * @param renderCell
	 *            to be added to the master list
	 */
	public void add(RenderCell renderCell) {
		if (!renderCellMasterList.contains(renderCell)) {
			renderCellMasterList.add(renderCell);
		}
	}
}
