package assudoku.gui;
import java.awt.*;

/**
 * The layout administering the font, cell colors and font colors for the
 * Sudoku.
 * 
 * @author tvho795, emilys, kler763
 */
public interface SudokuLayout {

	public static final int ERROR = -1;
	public static final int OVERLAP = -2;

	/**
	 * Sets the colors for the regions. Associates a color with a Region's int
	 * regionID value
	 * 
	 * @param regionId
	 *            int value representing the ID of a Region object
	 * @return color to paint cells in a region for the GUI
	 */
	public Color getGridColor(int regionId);

	/**
	 * Sets the color of the font to be used
	 * 
	 * @return the font color
	 */
	public Color getFontColor(boolean isEditable);

	/**
	 * Get the font
	 * 
	 * @return The particular font
	 */
	public Font getFont();

}
