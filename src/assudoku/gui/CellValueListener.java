package assudoku.gui;
/**
 * Interface for classes that need to perform actions once a text field in the
 * GUI has been updated, changed, or had its value deleted
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public interface CellValueListener {

	public void performOnCellValueChange(int x, int y);

}
