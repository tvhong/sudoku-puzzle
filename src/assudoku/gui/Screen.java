package assudoku.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.*;

import assudoku.core.SudokuPuzzle;
import assudoku.theme.FiveByFiveColors;
import assudoku.theme.PapermanColors;
import assudoku.theme.RainbowColors;

/**
 * The screen that is displayed Contains the mainFrame and mainPanel mainPanel
 * is continuously updated each time the user clicks on a button to traverse to
 * a new menu or the actual Sudoku game As each menu implements a screen
 * interface, multiple menus can be made need be.
 * 
 * @author tvho795, emilys, kler763
 */
public class Screen {
	private final JFrame mainFrame;
	private final JPanel mainPanel;
	private JPanel childPanel;
	private String sudokuFile;
	private SudokuPuzzle sudokuGame = null;
	private SudokuGui sudokuGui = null;
	private ArrayList<String> loadGameData;
	private SudokuMenuBar menuBar;

	public SudokuLayout colorScheme = new PapermanColors();
	public boolean errorHighlighting = false;

	/**
	 * Constructor of the screen class, initialises the mainFrame and mainPanel
	 * components
	 */
	public Screen() {
		mainPanel = new JPanel();
		mainFrame = new JFrame();
		childPanel = new JPanel();
		menuBar = new SudokuMenuBar(mainFrame, this);
		menuBar.optionsMenu.setEnabled(false);
		init();
	}

	/**
	 * Initialise the main frame and main panel
	 */
	private void init() {
		mainFrame.add(mainPanel);
		this.mainFrame.setMinimumSize(new Dimension(650, 720));
		this.mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.mainFrame.setVisible(true);
		this.mainFrame.add(mainPanel);

		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				quit();
			}
		});
	}

	/**
	 * Renders the screen, discards old panel and inserts specified panel
	 * 
	 * @param aPanel
	 *            a specific panel needed to be displayed
	 */
	private void renderScreen(JPanel aPanel) {
		// disables Options menu in menu bar when we change screen
		// as they are only applicable to when the sudoku puzzle is in play
		menuBar.optionsMenu.setEnabled(false);
		mainPanel.remove(childPanel);
		childPanel = aPanel;
		mainPanel.add(childPanel);
		SwingUtilities.updateComponentTreeUI(mainFrame); // Refreshes the frame
	}

	/**
	 * Return JFrame representing the main screen
	 * 
	 * @return JFrame
	 */
	public JFrame getMainFrame() {
		return this.mainFrame;
	}

	/**
	 * Changes the screen panel to the Sudoku Gui panel
	 */
	public void goToSudoku(String fileName) {
		this.sudokuFile = fileName;
		// default colour scheme to a b/w theme
		this.colorScheme = new PapermanColors();
		// set color scheme for this type of game
		setColorScheme(this.colorScheme);

		if (this.sudokuFile != null) {
			sudokuGame = new SudokuPuzzle(this.sudokuFile);
			sudokuGui = new SudokuGui(this.sudokuFile, sudokuGame,
					this.colorScheme, this.errorHighlighting);
			renderScreen(sudokuGui.getPanel());
			// enables Options menu in the menu bar when sudoku puzzle is in
			// play
			menuBar.optionsMenu.setEnabled(true);

			// if there's loaded game data, load it into the puzzle GUI
			if (this.loadGameData != null) {
				sudokuGui.loadSavedGameData(this.loadGameData);
			}
			// reset loaded game data so it does not conflict with new games
			this.loadGameData = null;
		} else {
			JOptionPane.showMessageDialog(new JFrame(),
					"Please choose size and difficulty");
		}
	}

	/**
	 * refresh Sudoku Gui with given colorScheme and errorHighlighting flag
	 */
	public void refreshSudokuAppearance() {
		setColorScheme(this.colorScheme);
		sudokuGui.drawGui(this.colorScheme, this.errorHighlighting);
	}

	/**
	 * Changes the screen panel to the difficulty/size menu
	 */
	public void goToDiffSet() {
		ScreenInterface diffSettings = new MenuLevelSelect(this);
		renderScreen(diffSettings.getPanel());
	}

	/**
	 * Changes the screen panel to the main menu
	 */
	public void goToMainMenu() {
		ScreenInterface mainMenu = new MenuMain(this);
		renderScreen(mainMenu.getPanel());
	}

	/**
	 * Prompts the user if they want to quit the game, if yes, frame closes
	 */
	public void quit() {
		int answer = JOptionPane.showConfirmDialog(mainFrame,
				"Are you sure you want to quit?", "Quit",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (answer == JOptionPane.YES_OPTION) {
			mainFrame.setVisible(false);
			mainFrame.dispose();
			System.exit(0);
		}
	}

	/**
	 * Updates the appearance settings
	 * 
	 * @param colorScheme
	 *            to apply to the current game
	 */
	public void setColorScheme(SudokuLayout colorScheme) {
		// some puzzle types require specific color themes - set theme for
		// puzzle type
		colorScheme = setThemeForPuzzleType(colorScheme);
		this.colorScheme = colorScheme;
	}

	/**
	 * Set a specific theme if it's a non-standard puzzle (eg. larger than 3x3,
	 * and not squares)
	 * 
	 * @param colorScheme
	 *            requested colorScheme to apply to the puzzle
	 * @return SudokuLayout containing colorScheme appropriate for this type of
	 *         puzzle
	 */
	private SudokuLayout setThemeForPuzzleType(SudokuLayout colorScheme) {
		if (this.sudokuFile.contains("/size4/")) {
			colorScheme = new FiveByFiveColors();
		}
		if (this.sudokuFile.contains("/jigsaw/")) {
			colorScheme = new RainbowColors();
		}
		return colorScheme;
	}

	/**
	 * Toggle error highlighting for this session
	 * 
	 * @param errorHighlighting
	 *            true means on, false means off
	 */
	public void setErrorHighlighting(boolean errorHighlighting) {
		this.errorHighlighting = errorHighlighting;
	}

	/**
	 * Load a sudoku puzzle from an existing saved game
	 */
	public void loadSaveGame() {
		File saveFile = new File(SudokuPuzzle.SAVE_GAME_FILEPATH);
		if (saveFile.exists()) {
			this.loadGameData = getSavedGameData(SudokuPuzzle.SAVE_GAME_FILEPATH);
			goToSudoku(this.sudokuFile);
		} else {
			JOptionPane.showMessageDialog(mainFrame,
					"You do not have a saved game");
		}
	}

	/**
	 * Return the an ArrayList of strings representing the user's entered cell
	 * data from a previously saved game
	 * 
	 * @return ArrayList of strings representing entered cell values
	 */
	public ArrayList<String> getSavedGameData(String saveFileName) {
		ArrayList<String> savedCellData = new ArrayList<String>();
		try {
			Scanner sc = new Scanner(new FileReader(saveFileName));

			// read game file name from first line
			this.sudokuFile = sc.nextLine();

			// reads subsequent lines of saveGameFile.txt containing cell value
			// information and stores them in an ArrayList
			while (sc.hasNextLine()) {
				savedCellData.add(sc.nextLine());
			}
		} catch (FileNotFoundException e) {
		}
		return savedCellData;
	}
}
