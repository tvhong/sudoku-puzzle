package assudoku.gui;
import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import java.util.Random;

import javax.swing.*;

import assudoku.core.FileNameGenerator;

/**
 * The menu for the sudoku type selection screen. Selects "Size" and
 * "Difficulty".
 * 
 * @author tvho795, emilys, kler763
 * 
 * 
 */
public class MenuLevelSelect implements ScreenInterface, ActionListener {

	private Screen parentScreen;
	private JPanel diffPanel;
	private ButtonGroup diffGroup;
	private ButtonGroup sizeGroup;
	private GridBagConstraints con;

	/**
	 * Constructs a menu screen for selecting sudoku puzzle preferences, has a
	 * reference to the main Screen object
	 * 
	 * @param parentScreen
	 *            the Screen object that contains/displays this menu screen
	 */
	public MenuLevelSelect(Screen parentScreen) {
		this.parentScreen = parentScreen;
		this.diffPanel = new JPanel();
		this.diffPanel.setLayout(new GridBagLayout());
		this.con = new GridBagConstraints();
		createMenu();
	}

	/**
	 * Creates components for a menu screen that allows users to select
	 * preferences for a sudoku puzzle, including size and difficulty/type
	 */
	@Override
	public void createMenu() {
		// Select size title
		setCon(0, 0);
		JLabel selectSize = new JLabel("Select size");
		selectSize.setFont(new Font("Arial", Font.BOLD, 30));
		diffPanel.add(selectSize, con);

		// Select size radio buttons
		sizeGroup = new ButtonGroup();
		addRadioButton("3 x 3", 0, 1, sizeGroup);
		addRadioButton("4 x 4", 0, 2, sizeGroup);
		// addRadioButton("5 x 5", 0, 3, sizeGroup);

		// Select difficulty title
		setCon(0, 4);
		JLabel selectDifficulty = new JLabel("Select difficulty");
		selectDifficulty.setFont(new Font("Arial", Font.BOLD, 30));
		diffPanel.add(selectDifficulty, con);

		// Select difficulty radio buttons
		diffGroup = new ButtonGroup();
		addRadioButton("Easy", 0, 5, diffGroup);
		addRadioButton("Medium", 0, 6, diffGroup);
		addRadioButton("Hard", 0, 7, diffGroup);
		addRadioButton("Jigsaw", 0, 8, diffGroup);

		// Initiate buttons
		JButton goButton = new JButton("Let's Go!");
		JButton randomButton = new JButton("Randomize");
		goButton.addActionListener(this);
		randomButton.addActionListener(this);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());

		setCon(0, 0);
		buttonPanel.add(goButton, con);

		setCon(1, 0);
		buttonPanel.add(randomButton, con);

		setCon(0, 9);
		con.fill = GridBagConstraints.HORIZONTAL;
		con.gridwidth = 2;
		diffPanel.add(buttonPanel, con);

	}

	/**
	 * Set the constraints
	 * 
	 * @param x
	 *            its x cell placement
	 * @param y
	 *            its y cell placement
	 */
	private void setCon(int x, int y) {
		con.gridx = x;
		con.gridy = y;
		// con.fill = 1;
		con.anchor = GridBagConstraints.LINE_START;
		con.insets = new Insets(10, 10, 10, 10);
	}

	/**
	 * Returns this JPanel object
	 */
	public JPanel getPanel() {
		return this.diffPanel;
	}

	/**
	 * Creates radio buttons and adds them to a given ButtonGroup
	 * 
	 * @param buttonName
	 *            name of the radio button
	 * @param x
	 *            GridBagConstraints.gridx value
	 * @param y
	 *            GridBagConstraints.gridy value
	 * @param buttonGroup
	 *            a ButtonGroup to add the JRadioButton to
	 */
	public void addRadioButton(String buttonName, int x, int y,
			ButtonGroup buttonGroup) {
		JRadioButton radioButton = new JRadioButton(buttonName);
		radioButton.addActionListener(this);
		buttonGroup.add(radioButton);
		setCon(x, y);
		diffPanel.add(radioButton, con);

	}

	/**
	 * Listener events for button actions
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Let's Go!")) {
			if (diffGroup.getSelection() != null
					&& sizeGroup.getSelection() != null) {
				int diffNum = 0;
				// get the difficulty button that is selected
				for (Enumeration<AbstractButton> buttons = diffGroup
						.getElements(); buttons.hasMoreElements();) {
					AbstractButton button = buttons.nextElement();
					if (button.isSelected()) {
						break;
					}
					diffNum++;
				}
				int sizeNum = 3; // default size is 3
				// Get the size button that is selected
				for (Enumeration<AbstractButton> buttons = sizeGroup
						.getElements(); buttons.hasMoreElements();) {
					AbstractButton button = buttons.nextElement();
					if (button.isSelected()) {
						break;
					}
					sizeNum++;
				}

				// generate a file from this
				FileNameGenerator fileGen = new FileNameGenerator();

				String fileName = fileGen.getFileName(sizeNum, diffNum);
				parentScreen.goToSudoku(fileName);
			} else {
				JOptionPane.showMessageDialog(parentScreen.getMainFrame(),
						"Please select size and difficulty");
			}
		}
		// random puzzle generator
		else if (e.getActionCommand().equals("Randomize")) {

			// randomize number for size
			Random rand = new Random();
			int minSize = 3;
			int maxSize = 4;

			int randomSize = rand.nextInt(maxSize - minSize + 1) + minSize;

			// randomize number for difficulty
			int minDiff = 0;
			int maxDiff = 3;

			int randomDiff = rand.nextInt(maxDiff - minDiff + 1) + minDiff;

			// Use the numbers generated to get a random sudoku
			FileNameGenerator fileGen = new FileNameGenerator();
			String fileName = fileGen.getFileName(randomSize, randomDiff);
			parentScreen.goToSudoku(fileName);

		}
	}

}