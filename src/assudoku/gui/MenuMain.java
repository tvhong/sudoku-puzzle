package assudoku.gui;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * Class for a main menu - start up menu splash screen with starting options
 * such as selecting New Game, Load Saved Game, or exit
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class MenuMain implements ScreenInterface, ActionListener {
	private Screen parentScreen;
	private JPanel startPanel;
	private GridBagConstraints con;

	/**
	 * Constructs a menu screen for the startup menu, has a reference to the
	 * main Screen object
	 * 
	 * @param screen
	 */
	public MenuMain(Screen screen) {
		parentScreen = screen;
		startPanel = new JPanel();
		startPanel.setLayout(new GridBagLayout()); // max 10 menu options
		con = new GridBagConstraints();
		createMenu();
	}

	/**
	 * Creates components for a menu screen that allows users to start a new
	 * sudoku game, or load an existing saved game, or to exit
	 */
	public void createMenu() {
		setCon(0, 0);
		JLabel space = new JLabel();
		startPanel.add(space, con);

		JLabel title = new JLabel("Awesome Sudoku");
		title.setFont(new Font("Arial", Font.BOLD, 30));
		setCon(0, 1);
		startPanel.add(title, con);
		addButton("New Game", 0, 2);
		addButton("Load Saved Game", 0, 3);
		addButton("Exit", 0, 4);

	}

	/**
	 * Set the constraints
	 * 
	 * @param x
	 *            its x cell placement
	 * @param y
	 *            its y cell placement
	 */
	private void setCon(int x, int y) {
		con.fill = 1;
		con.gridx = x;
		con.gridy = y;
		con.ipady = 20;
		con.insets = new Insets(50, 0, 0, 0);
	}

	/**
	 * Creates buttons with a given (x,y) position in a grid layout
	 * 
	 * @param buttonName
	 *            name of the button
	 * @param x
	 *            GridBagConstraints.gridx value
	 * @param y
	 *            GridBagConstraints.gridy value
	 */
	public void addButton(String buttonName, int x, int y) {
		JButton aButton = new JButton(buttonName);
		aButton.addActionListener(this);
		setCon(x, y);
		startPanel.add(aButton, con);
	}

	/**
	 * Returns this JPanel object
	 */
	@Override
	public JPanel getPanel() {
		return this.startPanel;
	}

	/**
	 * Listener events for button actions
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String aCommand = e.getActionCommand();

		if ("New Game".equals(aCommand)) {
			parentScreen.goToDiffSet();
		} else if ("Load Saved Game".equals(aCommand)) {
			parentScreen.loadSaveGame();
		} else if ("Exit".equals(aCommand)) {
			parentScreen.quit();
		}

	}

}