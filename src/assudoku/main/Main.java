package assudoku.main;
import assudoku.gui.Screen;

/**
 * Main class for running a Sudoku Game
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class Main {
	/**
	 * Main method calls a run method that will start our sudoku game
	 */
	public static void main(String args[]) {
		run();
	}

	/**
	 * Initialises the sudoku main menu screen which is the start of our game
	 */
	public static void run() {
		Screen screen = new Screen();
		screen.goToMainMenu();
	}
}
