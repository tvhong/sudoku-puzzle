package assudoku.theme;

import java.awt.Color;
import java.awt.Font;

import assudoku.gui.SudokuLayout;

/**
 * Five by Five colors - works on puzzles of size 3x3 and 4x4
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class FiveByFiveColors implements SudokuLayout {

	private final Color[] colorArray = { new Color(215, 215, 245),
			new Color(210, 227, 255), new Color(228, 192, 240),
			new Color(175, 202, 219), new Color(255, 255, 255) };

	public Color getGridColor(int regionId) {

		Color color = Color.WHITE;
		if (regionId == SudokuLayout.ERROR) {
			color = Color.PINK;
		} else if (regionId == SudokuLayout.OVERLAP) {
			color = Color.lightGray;
		} else {
			int i = regionId % 5;
			color = colorArray[i];
		}
		return color;
	}

	@Override
	public Color getFontColor(boolean isEditable) {
		if (isEditable) {
			return Color.DARK_GRAY;
		} else {
			return new Color(0, 55, 128);
		}
	}
	
	@Override
	public Font getFont() {
		Font cellFont = new Font("Arial", Font.BOLD, 40);
		return cellFont;
	}

}
