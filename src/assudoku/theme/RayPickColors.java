package assudoku.theme;

import java.awt.Color;
import java.awt.Font;

import assudoku.gui.SudokuLayout;

/**
 * Ray's original choice of colors - slightly modified by Em :)
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class RayPickColors implements SudokuLayout {

	public Color getGridColor(int regionID) {
		Color col = new Color(0, 151, 240);

		if (regionID % 2 == 0) {
			col = new Color(240, 88, 0);
		}

		if (regionID == SudokuLayout.ERROR) {
			col = new Color(255, 110, 200);
		}

		return col;
	}

	@Override
	public Color getFontColor(boolean isEditable) {
		if (isEditable) {
			return Color.WHITE;
		}
		return new Color(212, 212, 212);
	}

	@Override
	public Font getFont() {
		Font cellFont = new Font("Arial", Font.BOLD, 40);
		return cellFont;
	}

}
