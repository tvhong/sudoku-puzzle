package assudoku.theme;

import java.awt.Color;
import java.awt.Font;

import assudoku.gui.SudokuLayout;

/**
 * Set's the layout of the Sudoku to have a very DARK color scheme
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class OblivionColors implements SudokuLayout {

	public Color getGridColor(int regionID) {
		Color col = Color.DARK_GRAY;

		if (regionID % 2 == 0) {
			col = Color.BLACK;
		}

		if (regionID == SudokuLayout.ERROR) {
			col = new Color(130, 0, 0);
		}
		return col;
	}

	@Override
	public Color getFontColor(boolean isEditable) {
		if (isEditable) {
			return Color.WHITE;
		}
		return Color.GRAY;
	}

	@Override
	public Font getFont() {
		Font cellFont = new Font("Arial", Font.BOLD, 40);

		return cellFont;
	}

}
