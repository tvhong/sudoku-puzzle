package assudoku.theme;

import java.awt.Color;
import java.awt.Font;

import assudoku.gui.SudokuLayout;

/**
 * Pastel girly rainbow coloured theme - suitable for jigsaw sudokus of up to 9
 * regions
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class RainbowColors implements SudokuLayout {

	private final Color[] colorArray = { new Color(204, 255, 255),
			new Color(255, 255, 204), new Color(204, 204, 255),

			new Color(229, 255, 204), new Color(153, 204, 255),
			new Color(204, 255, 229),

			new Color(229, 204, 255), new Color(255, 229, 204),
			new Color(204, 229, 255) };

	public Color getGridColor(int regionId) {
		Color color = Color.WHITE;
		if (regionId == SudokuLayout.ERROR) {
			color = Color.PINK;
		} else if (regionId == SudokuLayout.OVERLAP) {
			color = Color.lightGray;
		} else {
			int i = regionId % 9;
			color = colorArray[i];
		}
		return color;
	}

	@Override
	public Color getFontColor(boolean isEditable) {
		if (isEditable) {
			return Color.DARK_GRAY;
		} else {
			return new Color(0, 55, 128);
		}
	}

	@Override
	public Font getFont() {
		Font cellFont = new Font("Arial", Font.BOLD, 40);
		return cellFont;
	}

}
