package assudoku.theme;

import java.awt.Color;
import java.awt.Font;

import assudoku.gui.SudokuLayout;

/**
 * Simplistic B&W theme - inspired by the puzzle sections in Sunday newspapers and Disney's Paperman short
 * 
 * @author tvho795, emilys, kler763
 * 
 */
public class PapermanColors implements SudokuLayout {

	public Color getGridColor(int regionID) {
		Color col = Color.LIGHT_GRAY;

		if (regionID % 2 == 0) {
			col = Color.WHITE;
		}

		if (regionID == SudokuLayout.ERROR) {
			col = Color.PINK;
		}
		return col;
	}

	@Override
	public Color getFontColor(boolean isEditable) {
		if (isEditable) {
			return Color.BLACK;
		}
		return Color.DARK_GRAY;
	}

	@Override
	public Font getFont() {
		// TODO Auto-generated method stub
		Font cellFont = new Font("Arial", Font.BOLD, 40);

		return cellFont;
	}

}
