import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;


public class SudokuTranslator {
   private char[][] sudoku = new char[64][64];
   private int size;
   private int n;
   
   public static void main(String args[]) {
      if (args.length < 1) {
         System.out.println("Usage: ./programName <input>\n");
         System.exit(1);
      }
      SudokuTranslator sudokuTranslator = new SudokuTranslator();
      sudokuTranslator.run(args[0]);
   }

   private void run(String fin) {

      try {
         Scanner sc = new Scanner(new FileReader(fin));
         //read the first line to dertermine the n
         size = sc.nextInt();
         n = size*size;

         //read values list
         System.out.print("Values ");
//         sc.nextLine(); //skip the 1st line's '\n'
         sc.skip("\n");
         System.out.println(sc.nextLine());

         sc.skip("\n");
         //read the rest which is the puzzle
         for (int y = 0; y < n; y++) {
            String[] elements = sc.nextLine().split(" ");
            for (int x = 0; x < n; x++) {
               sudoku[y][x] = elements[x].charAt(0);
            }
         }

/*         //DEBUGGING
         for (int y = 0; y < n; y++) {
            for (int x = 0; x < n; x++) {
               System.out.print(sudoku[y][x]);
            }
            System.out.println();
         }
*/
         //start parsing the input in term of regions
         for (int ytop = 0; ytop < size; ytop++) {
            for (int xtop = 0; xtop < size; xtop++) {
               generateRegion(ytop*size, xtop*size);
            }
         }

         sc.close();
      } catch (FileNotFoundException e) {
         System.err.println("FileNotFoundException:" + e.getMessage());
      }
   }

   private void generateRegion(int y, int x) {
      System.out.println("Region");
      for (int dy = 0; dy < size; dy++) {
         for (int dx = 0; dx < size; dx++) {
            int newx = x + dx;
            int newy = y + dy;
            System.out.print(newx + " " + newy);
            if (sudoku[newy][newx] != '0') {
               System.out.print(" " + sudoku[newy][newx]);
            }
            System.out.println();
         }
      }
   }

}
