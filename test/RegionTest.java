import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class RegionTest {
    final int N = 3;

    @Test
    public void testAddCell() {
        Region<Integer> region = new Region<Integer>(N);
        for (int i = 0; i < N*N; i++) { //add n^2 values to the region
            Cell<Integer> cell = new Cell<Integer>(i);
            assertTrue(region.addCell(cell));
        }
        
        assertFalse("The region is now full, add one more cell would cause an error",
                    region.addCell(new Cell<Integer>(9)));
    }
    
    @Test
    public void testAddCells() {
        Region<Integer> region = new Region<Integer>(N);
        ArrayList<Cell<Integer>> tmp = new ArrayList<Cell<Integer>>();
        for (int i = 0; i < N*N; i++) { //add n^2 values to the region
            Cell<Integer> cell = new Cell<Integer>(i);
            tmp.add(cell);
        }
        assertTrue(region.addCells(tmp));
        
        tmp = new ArrayList<Cell<Integer>>();
        tmp.add(new Cell<Integer>(100));
        assertFalse("The region is now full, add more cell(s) would cause an error",
                    region.addCells(tmp));
    }
    
    @Test
    public void testCheckBlock() {
        Region<Integer> block1 = new Region<Integer>(N);
        for (int i = 1; i <= N*N; i++) {
            Cell<Integer> cell = new Cell<Integer>(i);
            assertTrue(block1.addCell(cell));
        }
        assertTrue("The region should contain unique values", block1.checkBlock());
        
        Region<Integer> block2 = new Region<Integer>(N);
        for (int i = 1; i < N*N; i++) { // add only (N*N - 1) cells
            Cell<Integer> cell = new Cell<Integer>(i);
            assertTrue(block2.addCell(cell));
        }
        block2.addCell(new Cell<Integer>(1));
        assertFalse("There are two 1's in the region, checkBlock should fail",
                    block2.checkBlock());
    }
    
    @Test
    public void testToString() {
        Region<Integer> region = new Region<Integer>(N);
        for (int i = 1; i <= N*N; i++) {
            Cell<Integer> cell = new Cell<Integer>(i);
            assertTrue(region.addCell(cell));
        }
        assertEquals("_1_2_3_4_5_6_7_8_9", region.toString());
    }
}
