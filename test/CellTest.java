import static org.junit.Assert.*;

import org.junit.Test;


public class CellTest {

    @Test
    public void testGetValue() {
        Cell<Integer> cell = new Cell<Integer>(1);
        Integer expected = new Integer(1);
        assertEquals(expected, cell.getValue());
    }
    
    @Test
    public void testSetValue() {
        //assume that cell.getValue() work correctly.
        Cell<Integer> cell = new Cell<Integer>(1);
        Integer expected = new Integer(100);
        assertTrue("Set value must success", cell.setValue(100));
        assertEquals(expected, cell.getValue());
    }
    
    @Test
    public void testLock() {
        Cell<Integer> cell = new Cell<Integer>(1);
        cell.lock();
        assertFalse("Cell is locked, set value must fail", cell.setValue(100));
    }
}
