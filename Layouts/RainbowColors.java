

import java.awt.Color;
import java.awt.Font;


public class RainbowColors implements SudokuLayout {

	public Color getGridColor(int region) {
		Color col = Color.GRAY;
		if (region == 0) {
			col = Color.ORANGE;
		} else if (region == 1) {
			col = Color.BLUE;
		} else if (region == 2) {
			col = Color.GREEN;
		} else if (region == 3) {
			col = Color.MAGENTA;
		} else if (region == 4) {
			col = Color.PINK;
		} else if (region == 5) {
			col = Color.WHITE;
		} else if (region == 6) {
			col = Color.YELLOW;
		} else if (region == 7) {
			col = Color.ORANGE;
		} else if (region == 8) {
			col = Color.CYAN;
		}
		
		return col;
	}
	@Override
	public Font getFont(int regionContainer) {
		Font cellFont = new Font("Arial", Font.BOLD, 40);
		
		return cellFont;
	}
	@Override
	public Color getFontColor() {
		return Color.BLACK;
	}
}
