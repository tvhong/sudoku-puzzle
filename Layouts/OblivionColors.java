

import java.awt.Color;
import java.awt.Font;


public class OblivionColors implements SudokuLayout{

	public Font getFont(int regionContainer) {
		Font cellFont = new Font("Arial", Font.BOLD, 40);

		return cellFont;
	}

	public Color getGridColor(int regionContainer) {
		Color col = Color.DARK_GRAY;
		
		if(regionContainer % 2 == 0) {
			col = Color.BLACK;
		}
		return col;
	}

	@Override
	public Color getFontColor() {
		return Color.WHITE;
	}

}
